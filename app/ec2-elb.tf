# Data sources to get VPC and subnet
data "aws_vpc" "Demo" {
  tags {
    Name = "Demo"
  }
}

data "aws_subnet" "public_subnet" {
  tags {
    Name = "public_subnet6"
  }
}

# Create security group for ELB
resource "aws_security_group" "sg_elb" {
  description = "Security group for ELB"
  name = "sg_elb"
  vpc_id = "${data.aws_vpc.Demo.id}"

  tags {
    Name = "SG-ELB"
  }
}

# Create security group for EC2
resource "aws_security_group" "sg_ec2" {
  description = "Security group for EC2 instance"
  name = "sg_ec2"
  vpc_id = "${data.aws_vpc.Demo.id}"

  tags {
    Name = "SG-EC2"
  }
}

# Add rules to SG-ELB
resource "aws_security_group_rule" "allow_http" {
  description = "Allow http traffic"
  from_port = 80
  protocol = "tcp"
  security_group_id = "${aws_security_group.sg_elb.id}"
  to_port = 80
  type = "ingress"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "allow_all_traffic" {
  description = "Allow all traffic from SG-ELB to SG-EC2"
  from_port = 0
  protocol = "all"
  security_group_id = "${aws_security_group.sg_elb.id}"
  to_port = 0
  type = "egress"
  source_security_group_id = "${aws_security_group.sg_ec2.id}"
}

# Add rules to SG-EC2
resource "aws_security_group_rule" "allow_ssh" {
  description = "All ssh"
  from_port = 22
  protocol = "tcp"
  security_group_id = "${aws_security_group.sg_ec2.id}"
  to_port = 22
  type = "ingress"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "allow_http_ec2" {
  from_port = 80
  protocol = "tcp"
  security_group_id = "${aws_security_group.sg_ec2.id}"
  to_port = 80
  type = "ingress"
  source_security_group_id = "${aws_security_group.sg_elb.id}"
}

resource "aws_security_group_rule" "allow_all_ec2" {
  from_port = 0
  protocol = "all"
  security_group_id = "${aws_security_group.sg_ec2.id}"
  to_port = 0
  type = "egress"
  cidr_blocks = ["0.0.0.0/0"]
}

# Create a new instance with Ubuntu 16.04 on an
# t2.micro node with an AWS Tag naming it "demo-app-01"
resource "aws_instance" "my_ec2" {
  ami = "ami-0213e2640a5b2f74c"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.sg_ec2.id}"]
  subnet_id = "${data.aws_subnet.public_subnet.id}"

  tags {
    Name = "demo-app-01"
  }
}

# Create a new load balancer with an AWS Tag naming it "demo-app-01"
resource "aws_elb" "elb" {
  "listener" {
    instance_port = 80
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }

  name = "demo-app-01"
  subnets = ["${data.aws_subnet.public_subnet.id}"]
  security_groups = ["${aws_security_group.sg_elb.id}"]

  tags {
    Name = "demo-app-01"
  }
}

# Create a new load balancer attachment
resource "aws_elb_attachment" "my_elb-ec2" {
  elb = "${aws_elb.elb.id}"
  instance = "${aws_instance.my_ec2.id}"
}

# Simple output
output "EC2 instance public ip" {
  value = ["${aws_instance.my_ec2.public_ip}"]
}

output "ELB public dns name" {
  value = ["${aws_elb.elb.dns_name}"]
}