#Create S3 bucket
resource "aws_s3_bucket" "mybucket" {
  bucket = "mybucket112234"

  versioning {
    enabled = false
  }

  tags {
    Name = "nginx-configuration"
  }
}