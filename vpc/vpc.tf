# Define availability zones
variable "subnet" {
  type = "list"

  default = [
    "us-east-1a",
    "us-east-1b",
    "us-east-1c",
    "us-east-1d",
    "us-east-1e",
    "us-east-1f",
  ]
}

# Define VPC
resource "aws_vpc" "Demo" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags {
    Name = "Demo"
  }
}

# Define private subnets
resource "aws_subnet" "private_subnet" {
  count             = 6
  cidr_block        = "10.0.${count.index}.0/24"
  vpc_id            = "${aws_vpc.Demo.id}"
  availability_zone = "${element(var.subnet, count.index)}"

  tags {
    Name = "private_subnet${count.index+1}"
  }
}

# Define public subnets
resource "aws_subnet" "public_subnet" {
  count                   = 6
  cidr_block              = "10.0.${count.index+10}.0/24"
  vpc_id                  = "${aws_vpc.Demo.id}"
  availability_zone       = "${element(var.subnet, count.index)}"
  map_public_ip_on_launch = true

  tags {
    Name = "public_subnet${count.index+1}"
  }
}

# Define the internet gateway
resource "aws_internet_gateway" "gateway" {
  vpc_id = "${aws_vpc.Demo.id}"

  tags {
    Name = "VPC IGW"
  }
}

# Define the route table
resource "aws_route_table" "public_subnet" {
  vpc_id = "${aws_vpc.Demo.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gateway.id}"
  }

  tags {
    Name = "Public subnet route"
  }
}

# Assign the route table to public subnets
resource "aws_route_table_association" "public_subnet" {
  count          = 6
  route_table_id = "${aws_route_table.public_subnet.id}"
  subnet_id      = "${element(aws_subnet.public_subnet.*.id, count.index)}"
}